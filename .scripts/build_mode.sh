#!/bin/bash
# Install dependencies
pip install -r requirements.txt
 
# Build the model
python src/train_model.py