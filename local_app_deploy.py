from flask import Flask, request, jsonify
import pickle
import torch
import logging
from datetime import datetime

app = Flask(__name__)

# Load tokenizer and model
with open('tokenizer.pkl', 'rb') as f:
    tokenizer = pickle.load(f)
with open('model.pkl', 'rb') as f:
    model = pickle.load(f)

# Set up logging
logging.basicConfig(filename='app.log', level=logging.INFO, format='%(asctime)s - %(message)s')

@app.route('/get_ner_tags', methods=['POST'])
def get_ner_tags():
    try:
        data = request.get_json()
        sentence = data['sentence']

        inputs = tokenizer(sentence, return_tensors='pt', padding=True, truncation=True)

        with torch.no_grad():
            outputs = model(**inputs)

        tags_converter = {
            'LABEL_0': 'B-O',
            'LABEL_1': 'B-AC',
            'LABEL_2': 'B-LF',
            'LABEL_3': 'I-LF',
        }
        predictions = outputs.logits.argmax(dim=-1)[0].tolist()
        predicted_tags = [tags_converter.get(model.config.id2label.get(p, "LABEL_UNKNOWN"), "LABEL_UNKNOWN")
                          for p in predictions if p != -100]

        # Log the input sentence and the predicted tags
        log_data = {
            'timestamp': datetime.utcnow().isoformat(),
            'sentence': sentence,
            'predicted_tags': predicted_tags
        }
        logging.info(f"Input: {sentence}, Predictions: {predicted_tags}")

        return jsonify({'tags': predicted_tags})

    except Exception as e:
        logging.error(f"Error: {str(e)}")
        return jsonify({'error': str(e)})

if __name__ == '__main__':
    app.run(debug=True)
