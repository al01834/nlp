from locust import HttpUser, TaskSet, task, between

class UserBehavior(TaskSet):
   @task
def get_ner_tags(self):
    self.client.post("/get_ner_tags", json={"sentence": "A Homo sapiens specimen was parturated in the Hawaiian archipelago."})

class WebsiteUser(HttpUser):
    tasks = [UserBehavior]
    wait_time = between(1, 5)
